  <!-- DataTables -->
  <div class="card mb-3">
		<div class="card-body">
			<div class="table-responsive">
				<table class="table table-hover" id="dataTable" width="100%" cellspacing="0">
					<thead>
						<tr>
              <th>Type Mobil</th>
              <th>Warna Mobil</th>
              <th>Tanggal Keluar</th>
              <th>Jam Keluar</th>
              <th>Jam Tiba</th>
              <th>Keterangan</th>
              <th>Aksi</th>
						</tr>
					</thead>
					<tbody>
						<?php foreach ($list->result() as $dt): ?>
						<tr>
							<td>
								<?php echo $dt->type_mobil; ?>
							</td>
							<td>
								<?php echo $dt->warna; ?>
              </td>
							<td>
								<?php echo $dt->tanggal; ?>
              </td>
              <td>
								<?php echo $dt->jam_keluar; ?>
              </td>
							<td>
								<?php echo $dt->jam_kembali; ?>
              </td>
							<td>
								<?php echo $dt->keterangan; ?>
              </td>
							<td width="250">
								<a href="#"
								 class="btn btn-small"><i class="fas fa-edit"></i> Edit</a>
								<!-- <a onclick="deleteConfirm('<?php //echo site_url('sumbangan/delete/').$dt->idDanamasuk; ?>')"
								 href="#!" class="btn btn-small text-danger"><i class="fas fa-trash"></i> Hapus</a> -->
								 <a onClick="return confirm('Are you sure you want to delete?')" href='<?php echo site_url('sumbangan/delete/').$dt->idDanamasuk; ?>' type='button' class='btn btn-small text-danger'><i class="fas fa-trash"></i> delete</a>
							</td>
						</tr>
						<?php endforeach; ?>
					</tbody>
        </table>
			</div>
		</div>
	</div>

	<script>
	function deleteConfirm(url){
		$('#btn-delete').attr('href', url);
		$('#deleteModal').modal();
	}
	</script>