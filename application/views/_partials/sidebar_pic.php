<!-- Sidebar -->
<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion toggled">
    <!-- Sidebar - Brand -->
    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="#">
      <div class="sidebar-brand-text mx-3">PIC</div>
    </a>

    <!-- Divider -->
    <hr class="sidebar-divider my-0">

    <!-- Nav Item - Tugas -->
      <li class="nav-item <?php echo $this->uri->segment(1) == 'tugas' || $this->uri->segment(1) == '' ? 'active': '' ?>">
        <a class="nav-link" href="<?php echo site_url('tugas') ?>">
          <i class="fas fa-fw fa-tachometer-alt"></i>
          <span>Buat Tugas</span></a>
      </li>

      <!-- Nav Item - Approval SK-Masuk -->
      <li class="nav-item <?php echo $this->uri->segment(2) == 'skmasuk' ? 'active': '' ?>">
        <a class="nav-link" href="<?php echo site_url('approval/skmasuk') ?>">
          <i class="fas fa-fw fa-envelope"></i>
          <span>Daftar Approval Sk-Masuk</span></a>
      </li>

      <!-- Nav Item - Approval SK-Keluar-->
      <li class="nav-item <?php echo $this->uri->segment(2) == 'skkeluar' ? 'active': '' ?>">
        <a class="nav-link" href="<?php echo site_url('approval/skkeluar') ?>">
          <i class="fas fa-fw fa-envelope"></i>
          <span>Daftar Approval Sk-Keluar</span></a>
      </li>

    <!-- Divider -->
    <hr class="sidebar-divider d-none d-md-block">

    <!-- Sidebar Toggler (Sidebar) -->
    <div class="text-center d-none d-md-inline">
      <button class="rounded-circle border-0" id="sidebarToggle"></button>
    </div>
</ul>