<?php if ($this->session->flashdata('success')): ?>
<div class="alert alert-success" role="alert">
	<?php echo $this->session->flashdata('success'); ?>
</div>
<?php endif; ?>

<div class="card mb-3">
    <div class="card-body">
        <?php
            $data = $list->result();
        ?>
        <form method="post" action="<?php echo site_url('datastok/edit_simpan/').$data[0]->nomor_rangka; ?> ">
            <div class="form-group">
            <label for="name">Tipe Mobil*</label>
                <select name="tipe_mobil" id="tipe_mobil" class="form-control" onchange="getWarnaMobil(true)" required>
                <option value="<?php echo $data[0]->tipe_mobil; ?>" selected><?php echo $data[0]->tipe_mobil; ?></option>
                <option disabled>-----------------------------------</option>
                    <?php foreach ($list_tipe_mobil->result() as $d): ?>
                        <option value="<?php echo $d->tipe_mobil; ?>"><?php echo $d->tipe_mobil; ?></option>
                    <?php endforeach; ?>
                </select>
            </div>
            <div class="form-group">
              <label for="name">Warna Mobil*</label>
                <select name="warna_mobil" id="warna_mobil" class="form-control"required>
                    <option value="<?php echo $data[0]->warna_mobil; ?>" selected><?php echo $data[0]->warna_mobil; ?></option>
                </select>
            </div>
            <div class="form-group">
              <label for="name">No. Rangka*</label>
                <!-- <input class="form-control" type="text" name="name" placeholder="Product name"> -->
                <input class="form-control" required type="text" name="nomor_rangka" value="<?php echo $data[0]->nomor_rangka; ?>"/>
            </div>
            <input class="btn btn-success" type="submit" value="SIMPAN">
            <a href="<?php echo site_url('datastok/list/'); ?>" class="btn btn-danger">CANCEL</a>
        </form>
    </div>
</div>

<div class="card-footer small text-muted">
	* Harus Diisi
</div>

<script>
  function getWarnaMobil(reset) {
      var tipe_mobil = document.getElementById("tipe_mobil").value;
      var warna_mobil = $('#warna_mobil');
      $.ajax({
          type: "GET",
          url: '<?php echo site_url('tugas/warnaMobil')?>/'+tipe_mobil,
          success: function(data){
              var first = '<option value="<?php echo $data[0]->warna_mobil; ?>" selected><?php echo $data[0]->warna_mobil; ?></option>';
              first += '<option disabled>-----------------------------------</option>';
              first += data;
              if (reset) {
                warna_mobil.html(data);
              } else {
                warna_mobil.html(first);
              }
          }
      });
  }
  window.onload = function () { getWarnaMobil() }
</script>