<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Skkeluar extends CI_Controller {
	public function __construct(){
		parent::__construct();
		if (!$this->login_model->logged_id()){
			redirect('Auth','refresh');
		}
	}
	
	public function index($id_sk_keluar)
	{
		$data['page'] = 'skkeluar/add';
		$data['sidebar'] = $this->session->userdata['_type'];
		$sql = "SELECT * FROM sk_keluar WHERE id_sk_keluar = $id_sk_keluar";
		$data['sk_keluar'] = $this->db->query($sql)->result_array();

		$sql = "SELECT nomor_rangka FROM data_stok WHERE tipe_mobil = '".$data['sk_keluar'][0]['tipe_mobil']."'";
		$sql .= " AND warna_mobil = '".$data['sk_keluar'][0]['warna_mobil']."' AND stok > 0 ORDER BY nomor_rangka ASC";
		$data['list_mobil'] = $this->db->query($sql);

		$data['list_driver'] = $this->getDriver($data['sk_keluar'][0]);
		
		$this->load->view('_partials/template', $data);
	}

	public function buatsk($id_sk_keluar){
		if (isset($_POST)) {
			$var = $this->session->userdata;
			$_POST['id_admin'] = $var['_user_id'];
			$_POST['status'] = 'TELAH DIBUAT';
			$this->db->where('id_sk_keluar', $id_sk_keluar);
			$update = $this->db->update('sk_keluar', $_POST);
			if($update) {
				$mobil['stok'] = 0;
				$this->db->where('nomor_rangka', $_POST['nomor_rangka']);
				$this->db->update('data_stok', $mobil);

				$this->db->where('id_sk_keluar', $id_sk_keluar);
				$getData = $this->db->get('sk_keluar')->result_array();
				$notif['dari'] = $this->session->userdata['_name'];
				$notif['untuk'] = $getData[0]['id_pic'];
				$notif['pesan'] = 'Pengajuan SK Keluar dengan id '. $id_sk_keluar.' telah dibuat oleh admin';
				$notif['tipe'] = 'info';
				$this->db->insert('user_notifikasi', $notif);

				$body = str_replace(' ', '%20', $notif['pesan']);
				$sql = "SELECT * FROM users WHERE id = ".$getData[0]['id_pic'];
				$data['email'] = $this->db->query($sql)->result_array();
				$email = str_replace('@','-at-', $data['email'][0]['email']);
				$urlEmail = site_url('email/index/'.$email.'/'.$body);
				fopen($urlEmail, "r");

				$this->session->set_flashdata('success', "STATUS PENGAJUAN SK BERHASIL DIUBAH");
			} else {
				$this->session->set_flashdata('error', "GAGAL MENGUBAH STATUS PENGAJUAN SK KELUAR");
			}
		}

		echo '<script type="text/javascript">
				    window.location.href="'.$_SERVER['HTTP_REFERER'].'";
				</script>';
	}

	public function approve($id_sk_keluar, $nomor_rangka){
		$data['status'] = 'APPROVED';
		$this->db->where('id_sk_keluar', $id_sk_keluar);
		$update = $this->db->update('sk_keluar', $data);
		if($update) {
			$mobil['stok'] = 0;
			$this->db->where('nomor_rangka', $nomor_rangka);
			$update = $this->db->update('data_stok', $mobil);
			if($update) {
				$this->db->where('id_sk_keluar', $id_sk_keluar);
				$getData = $this->db->get('sk_keluar')->result_array();
				$notif['dari'] = $this->session->userdata['_name'];
				$notif['untuk'] = $getData[0]['id_admin'];
				$notif['pesan'] = 'Pengajuan SK Keluar dengan id '. $id_sk_keluar.' telah di approve.';
				$notif['tipe'] = 'success';
				$this->db->insert('user_notifikasi', $notif);

				$body = str_replace(' ', '%20', $notif['pesan']);
				$sql = "SELECT * FROM users WHERE id = ".$getData[0]['id_admin'];
				$data['email'] = $this->db->query($sql)->result_array();
				$email = str_replace('@','-at-', $data['email'][0]['email']);
				$urlEmail = site_url('email/index/'.$email.'/'.$body);
				fopen($urlEmail, "r");

				$this->session->set_flashdata('success', "SK BERHASIL DIAPPROVE");
			} else {
				$this->session->set_flashdata('error', "GAGAL MENGUBAH STOK MOBIL");
			}
		} else {
			$this->session->set_flashdata('error', "GAGAL MENGUBAH STATUS SK KELUAR");
		}

		echo '<script type="text/javascript">
				    window.location.href="'.$_SERVER['HTTP_REFERER'].'";
				</script>';
	}

	public function tolaksk($id_sk_keluar, $keterangan, $actor){
		$var = $this->session->userdata;
		if ($actor === 'id_pc') {
			$data['id_admin'] = $var['_user_id'];
		} else if ($actor === 'id_admin') {
			$data['id_pic'] = $var['_user_id'];
		}
		$data['status'] = 'DITOLAK';
		$data['keterangan'] =  str_replace('%20',' ',$keterangan);
		$this->db->where('id_sk_keluar', $id_sk_keluar);
		$update = $this->db->update('sk_keluar', $data);
		if($update) {
			if ($keterangan !== '---') {
				$mobil['stok'] = 1;
				$this->db->where('nomor_rangka', $keterangan);
				$this->db->update('data_stok', $mobil);
				$this->db->where('id_sk_keluar', $id_sk_keluar);
				$getData = $this->db->get('sk_keluar')->result_array();
				$notif['dari'] = $this->session->userdata['_name'];
				$notif['untuk'] = $getData[0][$actor];
				$notif['pesan'] = 'Pengajuan SK Keluar dengan id '. $id_sk_keluar.' ditolak.';
				$notif['tipe'] = 'danger';
				$this->db->insert('user_notifikasi', $notif);
			}

			$this->session->set_flashdata('success', "STATUS PENGAJUAN SK BERHASIL DIUBAH");
		} else {
			$this->session->set_flashdata('error', "GAGAL MENGUBAH STATUS PENGAJUAN SK KELUAR");
		}

		echo '<script type="text/javascript">
				    window.location.href="'.$_SERVER['HTTP_REFERER'].'";
				</script>';
	}

	public function getDriver($sk_keluar) {
		$sql = "SELECT * FROM users WHERE type = 'driver' ORDER BY name";
		$list_driver = $this->db->query($sql);
		$list_driver_available = [];
		foreach ($list_driver->result() as $driver):
			$checkDuty = $this->checkDriverDuty($driver->id, $sk_keluar);
			if ($checkDuty) {
				array_push($list_driver_available, $driver);
			}
		endforeach;
		return $list_driver_available;
	}
	public function checkDriverDuty($driver_id, $sk_keluar) {
		$sql = "SELECT * FROM sk_keluar WHERE tanggal = '".$sk_keluar['tanggal']."' AND (jam_kembali != '' OR jam_kembali != NULL) ";
		$sql .= "AND pembawa = ".$driver_id." AND status != 'DITOLAK' AND status != 'MENUNGGU DIBUAT ADMIN' ";
		$sql .= "AND ('".$sk_keluar['jam_keluar']."' >= jam_keluar OR '".$sk_keluar['jam_keluar']."' <= CAST(jam_kembali as time)) ";
		$sql .= "AND ('".$sk_keluar['jam_kembali']."' >= jam_keluar OR '".$sk_keluar['jam_kembali']."' <= CAST(jam_kembali as time)) ";
		$list_driver = $this->db->query($sql)->result();
		return (count($list_driver) == 0);
	}
}