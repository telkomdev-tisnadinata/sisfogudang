<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct(){
      parent::__construct();
  }

	private function hash_password($password){
   	return password_hash($password, PASSWORD_BCRYPT);
	}

	public function index(){
		$type = $this->login_model->logged_id();
		if ($type == 'admin') {
			redirect('datastok/');
		} elseif ($type == 'pic') {
			redirect('tugas/');
		} elseif ($type == 'security') {
			redirect('skmasuk/');
		}
		$this->load->view('login/main');
	}
	
	public function loginProcess(){
		//get data dari FORM
    $username = $this->input->post("username", TRUE);
    $password = MD5($this->input->post('password', TRUE));

   	//checking data via model
    $checking = $this->login_model->check_login(array('username' => $username), array('password' => $password));

		//jika ditemukan, maka create session
    if ($checking != FALSE) {
      $session_data = array(
          '_user_id'   => $checking[0]->id,
          '_user_name' => $checking[0]->username,
          '_name' => $checking[0]->name,
          '_type' => $checking[0]->type,
      );
      //set session userdata
      $this->session->set_userdata($session_data);
			$type = $checking[0]->type;
			if ($type == 'admin') {
				redirect('datastok/');
			} elseif ($type == 'pic') {
				redirect('tugas/');
			} elseif ($type == 'security') {
				redirect('skmasuk/');
			}
    } else {
    	//jika gagal maka akan masuk ke index
				echo '<script type="text/javascript">
						    alert(\'Login Gagal!\');
						</script>' ;
				echo '<script type="text/javascript">
						    window.location.href="'.$_SERVER['HTTP_REFERER'].'";
						</script>';
				// redirect('','refresh');
    }
	}

	public function logout() {
		$this->session->sess_destroy();
		redirect(site_url());
	}

}
