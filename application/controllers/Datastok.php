<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Datastok extends CI_Controller {
	public function __construct(){
		parent::__construct();
		if (!$this->login_model->logged_id()){
			redirect('Auth','refresh');
		}
	}
	
	public function index()
	{
		$data['page'] = 'datastok/add';
		$data['sidebar'] = $this->session->userdata['_type'];
		$sql = "SELECT tipe_mobil FROM data_stok GROUP BY tipe_mobil ORDER BY tipe_mobil ASC";
		$data['list_tipe_mobil'] = $this->db->query($sql);

		$this->load->view('_partials/template', $data);
	}
	public function list(){
		$data['page'] = 'datastok/list';
		$data['sidebar'] = $this->session->userdata['_type'];
		$data['list'] = $this->db->where('stok', 1);
		$data['list'] = $this->db->get('data_stok');
		$this->load->view('_partials/template', $data);
	}
	public function add(){
		if (isset($_POST)) {
			$_POST['stok'] = 1;
			$add = $this->db->insert('data_stok', $_POST);
			if($add) {
				$this->session->set_flashdata('success', "DATA STOK BERHASIL DITAMBAHKAN");
			} else {
				$this->session->set_flashdata('error', "GAGAL MENAMBAH DATA STOK");
			}
		}

		echo '<script type="text/javascript">
				    window.location.href="'.$_SERVER['HTTP_REFERER'].'";
				</script>';
	}
	public function edit($nomor_rangka){
		if (isset($_POST)) {
			$data['page'] = 'datastok/edit';
			$data['sidebar'] = $this->session->userdata['_type'];
			$this->db->where('nomor_rangka', $nomor_rangka);
			$data['list'] = $this->db->get('data_stok');
			$sql = "SELECT tipe_mobil FROM data_stok GROUP BY tipe_mobil ORDER BY tipe_mobil ASC";
			$data['list_tipe_mobil'] = $this->db->query($sql);
			$this->load->view('_partials/template', $data);
		}

	}
	public function edit_simpan($nomor_rangka){
		if (isset($_POST)) {
			$this->db->where('nomor_rangka', $nomor_rangka);
			$update = $this->db->update('data_stok', $_POST);
			if($update) {
				$this->session->set_flashdata('success', "DATA STOK BERHASIL DIUBAH");
			} else {
				$this->session->set_flashdata('error', "GAGAL MENGUBAH DATA STOK");
			}
		}
		echo '<script type="text/javascript">
		window.location.href="'.$_SERVER['HTTP_REFERER'].'";
	</script>';
	}
	public function delete($nomor_rangka){
		$this->db->where('nomor_rangka', $nomor_rangka);
		$delete = $this->db->delete('data_stok', $_POST);
		if($delete) {
			$this->session->set_flashdata('success', "DATA STOK BERHASIL DIHAPUS");
		} else {
			$this->session->set_flashdata('error', "GAGAL MENGHAPUS DATA STOK");
		}
		echo '<script type="text/javascript">
		window.location.href="'.$_SERVER['HTTP_REFERER'].'";
	</script>';
	}
}